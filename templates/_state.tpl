<hr>
    {% print m.acl.user %}
    {% print now %}
<hr>

{% if m.acl.user %}
    <h1>Hello, {{ m.acl.user.id.title }}</h1>

    {% button text="Logoff" postback={logoff} delegate=`drb_logon` %}
    
{% else %}

    <h2>Anon</h2>

    {% wire id="logon_form" type="submit" postback={logon} delegate=`drb_logon` %}
    <form id="logon_form" method="post" action="postback" class="z_logon_form" target="logonTarget">
        {% if not hide_title %}
            <h1 class="logon_header">{_ Log on to _} <span>{{ m.config.site.title.value|default:"Zotonic" }}</span>.</h1>
        {% endif %}
        
        <input type="hidden" name="page" value="{{ page|escape }}" />
        <input type="hidden" name="handler" value="username" />

        <div class="form-group">
            <label for="username" class="control-label">{_ Username _}</label>
            <div>
	            <input class="form-control" type="text" id="username" name="username" value="admin" autofocus="autofocus" autocapitalize="off" autocomplete="on" />
                {% validate id="username" type={presence} %}
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="control-label">{_ Password _}</label>
            <div>
	            <input class="form-control" type="password" id="password" name="password" value="admin" autocomplete="on" />
            </div>
        </div>

	            <button class="btn btn-primary btn-lg pull-right" type="submit">{_ Log on _}</button>
        
    </form>

{% endif %}
