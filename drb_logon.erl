-module(drb_logon).

-export([event/2]).

-include_lib("include/zotonic.hrl").


event(#submit{message={logon, []}}, Context) ->

    LogonArgs = z_context:get_q_all(Context),
    case z_notifier:first(#logon_submit{query_args=LogonArgs}, Context) of
        {ok, UserId} when is_integer(UserId) ->
            ContextUser = logon_user(UserId, Context),
            z_render:wire([{replace, [{target, "state"}, {template, "_state.tpl"}]}], ContextUser)
    end;

event(#postback{message={logoff, []}}, Context) ->
    lager:warning("off: ~p", [off]),
    ContextLogoff = z_auth:logoff(Context),
    z_render:wire([{replace, [{target, "state"}, {template, "_state.tpl"}]}], ContextLogoff).



logon_user(UserId, Context) ->
    case z_auth:logon(UserId, Context) of
		{ok, ContextUser} ->
		    ContextRemember = case z_context:get_q("rememberme", ContextUser, []) of
		        [] -> ContextUser;
		        _ -> controller_logon:set_rememberme_cookie(UserId, ContextUser)
		    end,
            ContextRemember
	end.
